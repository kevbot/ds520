#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// All instruction encoding formats
enum Format {
	Op,
	Op_Addr,
	Op_Reg,
	Op_Reg_Const,
	Op_Reg_Addr,
	Op_Reg_Reg,
	Op_Reg_Offset_Reg,
	Op_Reg_Reg_Addr
};

// Instruction name and its encoding format
struct Opcode {
	enum Format format;
	const char *opcode;
};

// stores list of instructions with index representing opcode
#define NUM_OPCODES 26
struct Opcode opcodes[] = {
	{Op, "halt"},
	{Op_Reg_Addr, "load"},
	{Op_Reg_Addr, "store"},
	{Op_Reg_Const, "ldimm"},
	{Op_Reg_Addr, "ldaddr"},
	{Op_Reg_Offset_Reg, "ldind"},
	{Op_Reg_Offset_Reg, "stind"},
	{Op_Reg_Reg, "addf"},
	{Op_Reg_Reg, "subf"},
	{Op_Reg_Reg, "divf"},
	{Op_Reg_Reg, "mulf"},
	{Op_Reg_Reg, "addi"},
	{Op_Reg_Reg, "subi"},
	{Op_Reg_Reg, "divi"},
	{Op_Reg_Reg, "muli"},
	{Op_Addr, "call"},
	{Op, "ret"},
	{Op_Reg_Reg_Addr, "blt"},
	{Op_Reg_Reg_Addr, "bgt"},
	{Op_Reg_Reg_Addr, "beq"},
	{Op_Addr, "jmp"},
	{Op_Reg_Reg_Addr, "cmpxchg"},
	{Op_Reg, "getpid"},
	{Op_Reg, "getpn"},
	{Op_Reg, "push"},
	{Op_Reg, "pop"},
};

// Errors returned from reading words from file
enum Error { None,		// No errors
			 EndOfFile, // When first byte of word is read
			 QuarterWord,
			 HalfWord,
			 ThreeQuarterWord };

// Disassembler mode
// Normal parses the header, symbols, and object code, stopping when either EOF or Object Code size is up (whichever occurs first)
// OnlyObjectCode treate the entire file as just object code, reading words until EOF. This can be useful when symbol encoding is broken.
// AllObjectCode is like normal mode, but will read until EOF, even if beyond header specified object code size.
enum Mode { Normal,
			OnlyObjectCode,
			AllObjectCode };

// Symbol is either Insymbol or Outsymbol
enum SymbolType { Insymbol,
				  Outsymbol };

// Values specified in header of as520 object file.
struct header {
	int insize;	 // size of insymbol section
	int outsize; // size of outsymbol section
	int objsize; // size of object code section
};

// Word read in from read_word
// word stores the actual value read in
// error keeps track of any errors encountered when reading in the word
struct word {
	enum Error error;
	uint32_t word;
};

// Symbol has a 16 byte name, an address, and a type (Insymbol or Outsymbol)
struct symbol {
	char name[16]; // when printing set precision to 16, else memory error.
	int address;
	enum SymbolType type;
};

// obj_file stores all data related to the given object file
struct obj_file {
	FILE *file;
	struct header header;
	int bytes;
	struct symbol *symbols;
};

typedef struct word Word;
typedef struct header HEAD;
typedef struct symbol SYMBOL;
typedef struct obj_file OBJ_FILE;

// reads in the next word (4 bytes, little endian) from the file.
// Returns the word and any errors it encountered.
Word read_word(OBJ_FILE *);

// Disassembles the file given by filename, using the given mode
// Reads in the object file specified by filename.
// See enum Mode documentation above for usage of mode.
void disassemble(char *filename, enum Mode mode);

// Reads the header of the object file, returning 0 on success. Not run if OnlyObjectCode is specified in disassemble.
// On any errors, an informative error is displayed, and if uncrecoverable, returns 1.
// If the file is sucessfully read without any errors, space is allocated for the symbol array which will be filled by parse_symbols.
// Errors: file ends before reading in the header info.
//         insymbol or outsymbol size is not correct (not a multiple of 5)
//         insymbol, outsymbol, object code size is greater than 2^20
int parse_header(OBJ_FILE *objfile);

// Reads symbols from object file, returning 0 on success. Not run if OnlyObjectCode is specified in disassemble.
// On any errors, an error is displayed and if unrecoverable, returns 1.
// Using the header info from parse_header, the symbols are read in and scanned for errors.
// Errors: file ends before reading correct number of symbols, returns 1.
//         symbol name is messed up, non-fatal error is shown, contunues parsing symbols.
int parse_symbols(OBJ_FILE *objfile);

// Reads object code from object file, returning 0 on success.
// On any unrecoverable errors, 1 is returned.
// If mode is OnlyObjectCode or AllObjectCode, the file is read until EOF.
// On Normal, the file is read until EOF or until object code size words are read, whichever comes first.
int parse_obj_code(OBJ_FILE *objfile, enum Mode mode);

// Compares two symbol names, returning 0 if equal.
int compare_symbols(char symbol1_name[16], char symbol2_name[16]);

// Prints the instruction.
// Replaces addresses with symbol names, if possible.
// Prints errors if encoding has issues.
// Note that errors may not be relevant as the instruction may be a word, and not an executed instruction.
void print_instruction(uint32_t word, int32_t address, SYMBOL *symbol_array, size_t array_size);

// Prints all symbols defined at given address.
void print_symbols(SYMBOL *symbols, size_t num_symbols, int address);

// searches through list of symbols for defined symbol at given address.
// Outsymbols match if Outsymbol address = pc,
// Insymbols match if Insymbol address = encoded_address + pc + 1.
// Returns symbol matching given address and pc value.
// Else returns null.
SYMBOL *find_symbol(SYMBOL *symbols, size_t num_symbols, int encoded_address, int pc);

// prints how to use the program
void usage(FILE *stream) {
	if (stream == NULL) {
		stream = stdout;
	}
	fprintf(stream, "Usage: ds520 [options] <object file(s)>\n"
					"Options: Only one can be given at a time\n\t-h - show this help message\n"
					"\t-s - treat all of file as object code "
					"(can be useful when symbolss are not being properly encoded)\n"
					"\t-a - disassemble all object code beyond size given in header\n");
}

int main(int argc, char *argv[]) {
	int start = 1;
	enum Mode mode = Normal;
	if (argc < 2) {
		usage(stderr);
		return 1;
	}

	if (strcmp(argv[1], "-o") == 0) {
		start = 2;
		mode = OnlyObjectCode;
		if (argc < 3) {
			usage(stderr);
		}
	} else if (strcmp(argv[1], "-a") == 0) {
		start = 2;
		mode = AllObjectCode;
		if (argc < 3) {
			usage(stderr);
		}
	} else if (strcmp(argv[1], "-h") == 0) {
		usage(stdout);
		return 0;
	}

	for (int i = start; i < argc; i++) {
		disassemble(argv[i], mode);
		if (i + 1 != argc) {
			printf("\n");
		}
	}

	return 0;
}

void disassemble(char *filename, enum Mode mode) {
	OBJ_FILE of;
	of.bytes = 0;

	printf("Dissassembly of %s\n", filename);

	of.file = fopen(filename, "r");
	if (of.file == NULL) {
		fprintf(stderr, "File %s not found.\n", filename);
		return;
	}

	if (mode != OnlyObjectCode) {
		if (parse_header(&of) != 0) {
			printf("Unrecoverable error parsing header, stopping.\n");
			fclose(of.file);
			return;
		}

		if (parse_symbols(&of) != 0) {
			printf("Unrecoverable error parsing symbols, stopping.\n");
			free(of.symbols);
			fclose(of.file);
			return;
		}
	}

	if (parse_obj_code(&of, mode) != 0) {
		printf("Unrecoverable error parsing header, stopping.\n");
		free(of.symbols);
		return;
	}

	if (mode != OnlyObjectCode) {
		free(of.symbols);
	}
	fclose(of.file);
}

// reads in the next word from the object file
// returns word with any error codes if present
// All possible errors are from the file ending
Word read_word(OBJ_FILE *of) {
	Word w;
	int c;
	w.error = None;

	// first byte
	c = getc(of->file);
	if (c == EOF) {
		w.error = EndOfFile;
		return w;
	}
	w.word = c;
	of->bytes++;

	// second byte
	c = getc(of->file);
	if (c == EOF) {
		w.error = QuarterWord;
		return w;
	}
	w.word += c << 8;
	of->bytes++;

	// third byte
	c = getc(of->file);
	if (c == EOF) {
		w.error = HalfWord;
		return w;
	}
	w.word += c << 16;
	of->bytes++;

	// fourth byte
	c = getc(of->file);
	if (c == EOF) {
		w.error = ThreeQuarterWord;
		return w;
	}
	w.word += c << 24;
	of->bytes++;

	return w;
}

int parse_header(OBJ_FILE *of) {
	Word w;
	// read in sizes

	// insymbol size
	w = read_word(of);
	switch (w.error) {
	case EndOfFile:
		printf("File is Empty\n");
		return 1;
	case QuarterWord:
		printf("Only one byte written to file\n");
		return 1;
	case HalfWord:
		printf("Only two bytes written to file\n");
		return 1;
	case ThreeQuarterWord:
		printf("Only three bytes written to file\n");
		return 1;
	default:
		of->header.insize = w.word;
	}

	// outsymbol size
	w = read_word(of);
	switch (w.error) {
	case EndOfFile:
		printf("Outsymbol size not written to file\n");
		return 1;
	case QuarterWord:
		printf("Only one byte written to file\n");
		return 1;
	case HalfWord:
		printf("Only two bytes written to file\n");
		return 1;
	case ThreeQuarterWord:
		printf("Only three bytes written to file\n");
		return 1;
	default:
		of->header.outsize = w.word;
	}

	// object code size
	w = read_word(of);
	switch (w.error) {
	case EndOfFile:
		printf("Object Code size not written to file\n");
		return 1;
	case QuarterWord:
		printf("Only one byte of object code size written to file\n");
		return 1;
	case HalfWord:
		printf("Only two bytes of object code size written to file\n");
		return 1;
	case ThreeQuarterWord:
		printf("Only three bytes of object code size written to file\n");
		return 1;
	default:
		of->header.objsize = w.word;
	}

	if (of->header.insize >= 0x100000) {
		printf("Insymbol size too large.\n");
		return 1;
	}
	if (of->header.outsize >= 0x100000) {
		printf("Outsymbol size too large.\n");
		return 1;
	}
	if (of->header.objsize >= 0x100000) {
		printf("Object code size too large.\n");
		return 1;
	}

	if (of->header.insize % 5 != 0) {
		printf("Insymbol size is not a multiple of 5.\n");
		return 1;
	}

	if (of->header.outsize % 5 != 0) {
		printf("Outsymbol size is not a multiple of 5.\n");
		return 1;
	}

	// may allocate around 200MB of space
	int symbol_count = of->header.insize + of->header.outsize;
	of->symbols = malloc(symbol_count / 5 * sizeof(SYMBOL));

	return 0;
}

int parse_symbols(OBJ_FILE *of) {
	int i, j, offset;
	Word w;
	SYMBOL symbol;
	int count = 0;

	// place outsymbols before insymbols
	// ensures find_symbol returns correct label
	// insert insymbols after outsymbols
	offset = of->header.outsize / 5;

	printf("Insymbols (%d)\n", of->header.insize / 5);
	for (i = 0; i < of->header.insize / 5; i++) {
		// 4 words for symbol name
		// 5th word stores address

		// first word
		w = read_word(of);
		switch (w.error) {
		case EndOfFile:
			printf("End of file when reading start of symbol name\n");
			return 1;
		case QuarterWord:
			printf("1/4 of word written when reading start of symbol name\n");
			return 1;
		case HalfWord:
			printf("1/2 of word written when reading start of symbol name\n");
			return 1;
		case ThreeQuarterWord:
			printf("3/4 of word written when reading start of symbol name\n");
			return 1;
		default:
			break;
		}
		symbol.name[0] = w.word;
		symbol.name[1] = w.word >> 8;
		symbol.name[2] = w.word >> 16;
		symbol.name[3] = w.word >> 24;

		// second word
		w = read_word(of);
		switch (w.error) {
		case EndOfFile:
			printf("End of file when reading second word of symbol name\n");
			return 1;
		case QuarterWord:
			printf("1/4 of word written when reading second word of symbol name\n");
			return 1;
		case HalfWord:
			printf("1/2 of word written when reading second word of symbol name\n");
			return 1;
		case ThreeQuarterWord:
			printf("3/4 of word written when reading second word of symbol name\n");
			return 1;
		default:
			break;
		}
		symbol.name[4] = w.word;
		symbol.name[5] = w.word >> 8;
		symbol.name[6] = w.word >> 16;
		symbol.name[7] = w.word >> 24;

		// third word
		w = read_word(of);
		switch (w.error) {
		case EndOfFile:
			printf("End of file when reading third word of symbol\n");
			return 1;
		case QuarterWord:
			printf("1/4 of word written when reading third word of symbol\n");
			return 1;
		case HalfWord:
			printf("1/2 of word written when reading third word of symbol\n");
			return 1;
		case ThreeQuarterWord:
			printf("3/4 of word written when reading third word of symbol\n");
			return 1;
		default:
			break;
		}
		symbol.name[8] = w.word;
		symbol.name[9] = w.word >> 8;
		symbol.name[10] = w.word >> 16;
		symbol.name[11] = w.word >> 24;

		// fourth word
		w = read_word(of);
		switch (w.error) {
		case EndOfFile:
			printf("End of file when reading fourth word of symbol\n");
			return 1;
		case QuarterWord:
			printf("1/4 of word written when reading fourth word of symbol\n");
			return 1;
		case HalfWord:
			printf("1/2 of word written when reading fourth word of symbol\n");
			return 1;
		case ThreeQuarterWord:
			printf("3/4 of word written when reading fourth word of symbol\n");
			return 1;
		default:
			break;
		}
		symbol.name[12] = w.word;
		symbol.name[13] = w.word >> 8;
		symbol.name[14] = w.word >> 16;
		symbol.name[15] = w.word >> 24;

		// address
		w = read_word(of);
		switch (w.error) {
		case EndOfFile:
			printf("End of file when reading address for symbol\n");
			return 1;
		case QuarterWord:
			printf("1/4 of word written when reading address for symbol\n");
			return 1;
		case HalfWord:
			printf("1/2 of word written when reading address for symbol\n");
			return 1;
		case ThreeQuarterWord:
			printf("3/4 of word written when reading address for symbol\n");
			return 1;
		default:
			break;
		}
		symbol.address = w.word;

		// copy to symbol array
		symbol.type = Insymbol;

		// index is total number in array + outsymbol size
		// skips outsymbo
		of->symbols[i + offset] = symbol;

		// print symbol and address
		printf("%.16s: %-7d ", symbol.name, symbol.address);
		if (symbol.address > 0x100000) {
			printf(" Error Symbol offset too large.");
		}

		// check if symbol name is encoded right
		// if symbol name doesn't exits
		if (symbol.name[0] == '\0') {
			printf(" Error in encoding of symbol name.");
		} else {
			// symbol name exists, but may be wrong

			// go to first null character
			for (j = 1; j < 16; j++) {
				if (symbol.name[j] == '\0') {
					break;
				}
			}
			// make sure all follwing are null
			for (; j < 16; j++) {
				if (symbol.name[j] != '\0') {
					// null character in middle of symbol name
					printf(" Error in encoding of symbol name.");
					break;
				}
			}
		}
		if (symbol.address > 0x100000) {
			printf(" Error Symbol offset too large.");
		}
		printf("\n");

		count++;
	}

	// insert outsymbols at beginning of array
	offset = 0;
	printf("Outsymbols (%d)\n", of->header.outsize / 5);
	for (i = 0; i < of->header.outsize / 5; i++) {
		// 4 words for symbol name
		// 5th word stores address

		// first word
		w = read_word(of);
		switch (w.error) {
		case EndOfFile:
			printf("End of file when reading start of symbol name\n");
			return 1;
		case QuarterWord:
			printf("1/4 of word written when reading start of symbol name\n");
			return 1;
		case HalfWord:
			printf("1/2 of word written when reading start of symbol name\n");
			return 1;
		case ThreeQuarterWord:
			printf("3/4 of word written when reading start of symbol name\n");
			return 1;
		default:
			break;
		}
		symbol.name[0] = w.word;
		symbol.name[1] = w.word >> 8;
		symbol.name[2] = w.word >> 16;
		symbol.name[3] = w.word >> 24;

		// second word
		w = read_word(of);
		switch (w.error) {
		case EndOfFile:
			printf("End of file when reading second word of symbol name\n");
			return 1;
		case QuarterWord:
			printf("1/4 of word written when reading second word of symbol name\n");
			return 1;
		case HalfWord:
			printf("1/2 of word written when reading second word of symbol name\n");
			return 1;
		case ThreeQuarterWord:
			printf("3/4 of word written when reading second word of symbol name\n");
			return 1;
		default:
			break;
		}
		symbol.name[4] = w.word;
		symbol.name[5] = w.word >> 8;
		symbol.name[6] = w.word >> 16;
		symbol.name[7] = w.word >> 24;

		// third word
		w = read_word(of);
		switch (w.error) {
		case EndOfFile:
			printf("End of file when reading third word of symbol\n");
			return 1;
		case QuarterWord:
			printf("1/4 of word written when reading third word of symbol\n");
			return 1;
		case HalfWord:
			printf("1/2 of word written when reading third word of symbol\n");
			return 1;
		case ThreeQuarterWord:
			printf("3/4 of word written when reading third word of symbol\n");
			return 1;
		default:
			break;
		}
		symbol.name[8] = w.word;
		symbol.name[9] = w.word >> 8;
		symbol.name[10] = w.word >> 16;
		symbol.name[11] = w.word >> 24;

		// fourth word
		w = read_word(of);
		switch (w.error) {
		case EndOfFile:
			printf("End of file when reading fourth word of symbol\n");
			return 1;
		case QuarterWord:
			printf("1/4 of word written when reading fourth word of symbol\n");
			return 1;
		case HalfWord:
			printf("1/2 of word written when reading fourth word of symbol\n");
			return 1;
		case ThreeQuarterWord:
			printf("3/4 of word written when reading fourth word of symbol\n");
			return 1;
		default:
			break;
		}
		symbol.name[12] = w.word;
		symbol.name[13] = w.word >> 8;
		symbol.name[14] = w.word >> 16;
		symbol.name[15] = w.word >> 24;

		// address
		w = read_word(of);
		switch (w.error) {
		case EndOfFile:
			printf("End of file when reading address for symbol\n");
			return 1;
		case QuarterWord:
			printf("1/4 of word written when reading address for symbol\n");
			return 1;
		case HalfWord:
			printf("1/2 of word written when reading address for symbol\n");
			return 1;
		case ThreeQuarterWord:
			printf("3/4 of word written when reading address for symbol\n");
			return 1;
		default:
			break;
		}
		symbol.address = w.word;

		symbol.type = Outsymbol;

		// print symbol and address
		printf("%.16s: %d", symbol.name, symbol.address);
		if (symbol.address > 0x100000) {
			printf(" Error Symbol offset too large.");
		}

		// check if symbol name is encoded right
		// if symbol name doesn't exits
		if (symbol.name[0] == '\0') {
			printf(" Error in encoding of symbol name.");
		} else {
			// symbol name exists, but may be wrong

			// go to first null character
			for (j = 1; j < 16; j++) {
				if (symbol.name[j] == '\0') {
					break;
				}
			}
			// make sure all follwing are null
			for (; j < 16; j++) {
				if (symbol.name[j] != '\0') {
					// null character in middle of symbol name
					printf(" Error in encoding of symbol name.");
					break;
				}
			}
		}
		if (symbol.address > 0x100000) {
			printf(" Error Symbol offset too large.");
		}
		printf("\n");

		// copy to symbol array
		// index is total number of elements in array - outsymbol section count
		of->symbols[i + offset] = symbol;

		count++;
	}

	for (i = 0; i < count; i++) {
		for (j = i + 1; j < count; j++) {
			if ((of->symbols[i].type != of->symbols[j].type) || (of->symbols[i].type == Insymbol && of->symbols[j].type == Insymbol)) {
				if (compare_symbols(of->symbols[i].name, of->symbols[j].name) == 0) {
					printf("Symbol \"%.16s\" is defined multiple times as ", of->symbols[i].name);
					switch (of->symbols[i].type) {
					case Insymbol:
						printf("insymbol");
						break;
					case Outsymbol:
						printf("outsymbol");
						break;
					}
					printf(" and as ");
					switch (of->symbols[j].type) {
					case Insymbol:
						printf("insymbol");
						break;
					case Outsymbol:
						printf("outsymbol");
						break;
					}
					printf("\n");
				}
			}
		}
	}

	return 0;
}

void print_symbols(SYMBOL *symbols, size_t num_symbols, int address) {
	int c, j;
	c = 0;
	for (j = 0; j < num_symbols; j++) {
		if (symbols[j].type == Insymbol && symbols[j].address == address) {
			if (c == 0) {
				printf("         | %.16s:\n", symbols[j].name);
				c = 1;
			} else {
				printf("         |          | %.16s:\n", symbols[j].name);
			}
		}
	}
	if (c != 0) {
		printf("         | ");
	}
}

int parse_obj_code(OBJ_FILE *of, enum Mode mode) {
	int i;
	Word w;
	int size = of->header.objsize;
	int num_symbols;

	switch (mode) {
	case OnlyObjectCode:
		printf("Object Code\n");
		size = 2 << 22;
		num_symbols = 0;
		break;
	default:
		printf("Object Code (%d)\n", size);
		num_symbols = (of->header.insize + of->header.outsize) / 5;
		break;
	}

	printf(" Address |   Word   |   Assembly and Errors\n");
	for (i = 0; i < size; i++) {

		w = read_word(of);
		switch (w.error) {
		case EndOfFile:
			if (mode != OnlyObjectCode) {
				printf("Error end of file. Object code file size is greater than actual file size.\n"
					   "Expected Size: %9d, Actual Size: %9d\n",
					   of->header.objsize, i);
				return 1;
			} else {
				return 0;
			}
			break;
		case QuarterWord:
			printf("Error 1/4 of word found at end of file.\n");
			return 1;
		case HalfWord:
			printf("Error 1/2 of word found at end of file.\n");
			return 1;
		case ThreeQuarterWord:
			printf("Error 3/4 of word found at end of file.\n");
			return 1;
		default:
			break;
		}

		printf("%8d | ", i); // print current address
		// print all symbols defined at this address
		print_symbols(of->symbols, num_symbols, i);
		print_instruction(w.word, i, of->symbols, num_symbols);
		printf("\n");
	}

	// check if at end of file
	w = read_word(of);

	// only continue if flags is specified
	if (mode == AllObjectCode && w.error != EndOfFile) {
		if (w.error == None) {
			printf("Warning: Continuing beyond object code size!\n");
		}
		while (w.error == None) {

			printf("%8d | ", i); // print current address
			print_symbols(of->symbols, num_symbols, i);
			print_instruction(w.word, i, of->symbols, num_symbols);
			printf("\n");

			w = read_word(of);
			i++;
		}

		// check what error was reached
		switch (w.error) {
		case EndOfFile:
			printf("Header claims object code size is %d but actual object code size is %d\n", of->header.objsize, i);
			break;
		case QuarterWord:
			printf("Error 1/4 of word found at end of file.\n");
			return 1;
		case HalfWord:
			printf("Error 1/2 of word found at end of file.\n");
			return 1;
		case ThreeQuarterWord:
			printf("Error 3/4 of word found at end of file.\n");
			return 1;
		default: // should be unreachable
			break;
		}
	} else {
		if (w.error != EndOfFile) {
			printf("Error, object code continues beyond given object code size. (run with -a to continue beyond size given in header)\n");
			return 1;
		}
	}

	return 0;
}

// compares if two symbols are the same, returns 0 if equal, index+1 of difference otherwise
int compare_symbols(char l1[16], char l2[16]) {
	for (int i = 0; i < 16; i++) {
		if (l1[i] < l2[i]) {
			return i + 1;
		} else if (l1[i] > l2[i]) {
			return -(i + 1);
		}
	}
	return 0;
}

SYMBOL *find_symbol(SYMBOL *symbols, size_t num_symbols, int addr, int pc) {
	size_t i;
	for (i = 0; i < num_symbols; i++) {
		switch (symbols[i].type) {
		case Insymbol:
			if (symbols[i].address == addr + pc + 1) {
				return &symbols[i];
			}
			break;
		case Outsymbol:
			if (symbols[i].address == pc) {
				return &symbols[i];
			}
			break;
		}
	}
	return NULL;
}

void print_instruction(uint32_t instruction, const int32_t pc, SYMBOL *symbols, const size_t num_symbols) {
	uint8_t op = instruction;
	uint8_t reg1, reg2;
	int32_t addr; // also used for offset
	SYMBOL *symbol;
	printf("%08x |   ", instruction);

	// print operation
	if (op > NUM_OPCODES) {
		printf("[Unknown Opcode] ");
	} else {
		printf("%-7s ", opcodes[op].opcode);
		switch (opcodes[op].format) {
		case Op:
			if (instruction != op) {
				printf("  # Bits 8-31 not 0");
			}
			break;
		case Op_Addr:
			addr = (int32_t)instruction >> 12;
			symbol = find_symbol(symbols, num_symbols, addr, pc);
			if (symbol == NULL) {
				printf("%d", addr);
			} else {
				printf("%.16s", symbol->name);
			}
			if ((instruction & 0xF00) != 0) {
				printf("  # Bits 8-11 not 0");
			}
			break;
		case Op_Reg:
			reg1 = (instruction >> 8) & 0xF;
			printf("r%d", reg1);
			if ((instruction & 0xFFFFF000) != 0) {
				printf("  # Bits 12-31 not 0");
			}
			break;
		case Op_Reg_Const:
			reg1 = (instruction >> 8) & 0xF;
			addr = (int32_t)instruction >> 12;
			printf("r%d, %d", reg1, addr);
			break;
		case Op_Reg_Addr:
			reg1 = (instruction >> 8) & 0xF;
			addr = (int32_t)instruction >> 12;
			symbol = find_symbol(symbols, num_symbols, addr, pc);
			if (symbol == NULL) {
				printf("r%d, %d", reg1, addr);
			} else {
				printf("r%d, %.16s", reg1, symbol->name);
			}
			break;
		case Op_Reg_Reg:
			reg1 = (instruction >> 8) & 0xF;
			reg2 = (instruction >> 12) & 0xF;
			printf("r%d, r%d", reg1, reg2);
			if ((instruction & 0xFFFF0000) != 0) {
				printf("  # Bits 16-31 not 0");
			}
			break;
		case Op_Reg_Offset_Reg:
			reg1 = (instruction >> 8) & 0xF;
			reg2 = (instruction >> 12) & 0xF;
			addr = (int32_t)instruction >> 16;
			printf("r%d, %d(%d)", reg1, addr, reg2);
			break;
		case Op_Reg_Reg_Addr:
			reg1 = (instruction >> 8) & 0xF;
			reg2 = (instruction >> 12) & 0xF;
			addr = (int32_t)instruction >> 16;
			symbol = find_symbol(symbols, num_symbols, addr, pc);
			if (symbol == NULL) {
				printf("r%d, r%d, %d", reg1, reg2, addr);
			} else {
				printf("r%d, r%d, %.16s", reg1, reg2, symbol->name);
			}
			break;
		}
	}
}
