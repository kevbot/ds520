CC=gcc
CFLAGS= -Wall -g

all: ds520

ds520: main.c
	$(CC) $(CFLAGS) -o $@ $^

clean:
	-rm -f ds520